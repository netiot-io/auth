package io.netiot.auth.generators

import io.netiot.auth.models.JwtUserModel

class JwtUserModelGenerator {

    static aJwtUserModel(Map overrides = [:]) {
        Map values = [
                id: 1,
                firstName: "Unit",
                lastName: "Test",
                email: "test@gmail.com",
                password: "password",
                active: Boolean.TRUE,
                role: "ADMIN",
                enabled: Boolean.TRUE,
                organizationId: 1L,
                organizationName: "OrganizationName",
        ]
        values << overrides
        return JwtUserModel.newInstance(values)
    }

}