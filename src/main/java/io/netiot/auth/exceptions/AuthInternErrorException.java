package io.netiot.auth.exceptions;

import org.springframework.security.oauth2.common.exceptions.ClientAuthenticationException;

public class AuthInternErrorException extends ClientAuthenticationException {

    public AuthInternErrorException(String msg) {
        super(msg);
    }
    @Override
    public String getOAuth2ErrorCode() {
        return "backend.auth.internal.error";
    }

}
