package io.netiot.auth.UT.services

import io.netiot.auth.feign.UserEndpoints
import io.netiot.auth.generators.JwtUserModelGenerator
import io.netiot.auth.models.SpringUserDetails
import io.netiot.auth.services.UserDetailsServiceImp
import org.springframework.security.core.userdetails.UsernameNotFoundException
import spock.lang.Specification

class UserDetailsServiceImpSpec extends Specification {


    def userEndpoints = Mock(UserEndpoints)
    def userDetailsServiceImp = new UserDetailsServiceImp(userEndpoints)

    def 'loadUserByUsername'(){
        given:
            def email = "test@gmail.com"
            def jwtUserModel = JwtUserModelGenerator.aJwtUserModel()

        when:
            SpringUserDetails springUserDetails = userDetailsServiceImp.loadUserByUsername(email)

        then:
            1 * userEndpoints.getJwtUser(email) >> [jwtUserModel]
            0 * _

            springUserDetails.getId() == jwtUserModel.getId()
            springUserDetails.getUsername() == jwtUserModel.getEmail()
            springUserDetails.getPassword() == jwtUserModel.getPassword()
            springUserDetails.getAuthorities().size() == 1
            springUserDetails.getAuthorities()[0].getAuthority() == jwtUserModel.getRole()
            springUserDetails.getRole() == jwtUserModel.getRole()
            springUserDetails.getFirstName() == jwtUserModel.getFirstName()
            springUserDetails.getLastName() == jwtUserModel.getLastName()
            springUserDetails.getActive() == jwtUserModel.getActive()
            springUserDetails.getOrganizationName() == jwtUserModel.getOrganizationName()
            springUserDetails.getOrganizationId() == jwtUserModel.getOrganizationId()
    }

    def 'loadUserByUsername email not exist'(){
        given:
            def email = "test@gmail.com"

        when:
            userDetailsServiceImp.loadUserByUsername(email)

        then:
            1 * userEndpoints.getJwtUser(email) >> []
            0 * _
            thrown(UsernameNotFoundException)
    }
}
