package io.netiot.auth.feign;

import io.netiot.auth.models.JwtUserModel;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserEndpointsFallback implements UserEndpoints {

    @Override
    public List<JwtUserModel> getJwtUser(final String email) {
        return null;
    }

}
