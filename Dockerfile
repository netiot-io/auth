FROM anapsix/alpine-java:8
ADD target/auth.jar auth.jar
ENV JAVA_OPTS=""
ENTRYPOINT exec java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /auth.jar
