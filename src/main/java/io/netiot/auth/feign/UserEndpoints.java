package io.netiot.auth.feign;

import io.netiot.auth.models.JwtUserModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "users-and-organizations", fallback = UserEndpointsFallback.class)
public interface UserEndpoints {

    @RequestMapping(method = RequestMethod.GET, value = "V1/user/internal/auth")
    List<JwtUserModel> getJwtUser(@RequestParam(name = "email") final String email);

}
