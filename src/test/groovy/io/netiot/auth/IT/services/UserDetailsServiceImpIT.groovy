package io.netiot.auth.IT.services

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestPropertySource
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(properties = ['spring.cloud.discovery.enabled=false',
        'spring.cloud.zookeeper.enabled=false',
        'spring.cloud.zookeeper.config.enabled=false'])
class UserDetailsServiceImpIT extends Specification {

//    @Autowired
//    UserDetailsServiceImp userDetailsServiceImp
//
//    @MockBean
//    UserEndpoints endpoints = Mock(UserEndpoints)
//
//    def 'loadUserByUsername email not exist'(){
//        given:
//            def email = "test@gmail.com"
//
//        when:
//            userDetailsServiceImp.loadUserByUsername(email)
//
//        then:
//        1 * endpoints.getJwtUser(email) >> Optional.empty()
//        0 * _
//
//        thrown(UsernameNotFoundException)
//    }

}
