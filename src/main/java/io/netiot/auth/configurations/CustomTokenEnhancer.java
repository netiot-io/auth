package io.netiot.auth.configurations;

import io.netiot.auth.models.SpringUserDetails;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.HashMap;
import java.util.Map;

public class CustomTokenEnhancer implements TokenEnhancer {

    @Override
    public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
        SpringUserDetails user = (SpringUserDetails) authentication.getPrincipal();
        final Map<String, Object> additionalInfo = new HashMap<>();

        additionalInfo.put("userId", user.getId());
        additionalInfo.put("firstName", user.getFirstName());
        additionalInfo.put("lastName", user.getLastName());
        additionalInfo.put("email", user.getEmail());
        additionalInfo.put("role", user.getRole());
        additionalInfo.put("organizationId", user.getOrganizationId());
        additionalInfo.put("organizationName", user.getOrganizationName());

        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

        return accessToken;
    }

}
