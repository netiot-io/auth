package io.netiot.auth.exceptions;

import org.springframework.security.oauth2.common.exceptions.ClientAuthenticationException;

public class UnactivatedUserException extends ClientAuthenticationException {

    public UnactivatedUserException(String msg) {
        super(msg);
    }

    @Override
    public String getOAuth2ErrorCode() {
        return "backend.auth.unactivated.user";
    }

}
