# Auth

Handles the user authentification with OAUTH2.

Performs the login of an user and returns its jwt token.

## Known paths
`GET auth/V1/oauth/token` 

Returns
```json
{
   "access_token":"",
   "token_type":"bearer",
   "refresh_token":"",
   "expires_in":43199,
   "scope":"all",
   "organizationId":5,
   "firstName":"",
   "lastName":"",
   "role":"PARTNER_USER",
   "organizationName":"PARTNER 2",
   "userId":7,
   "email":"",
   "jti":""
}
```

## Feign

Communicates with the `Users` microservice to GET the `user info` based on the given `user email`
`GET V1/user/internal/auth?email=`

Returns
```json
{
  "id": 1,
  "firstName": "",
  "lastName": "",
  "email": "",
  "password": "",
  "active": true,
  "enabled": true,
  "role": "",
  "organizationId": 1,
  "organizationName": ""
}
```

## CI/CD
Compile, test, package, build docker image and push to gitlab docker registry.

## Tests
Groovy tests in `src/test/groovy`.


 