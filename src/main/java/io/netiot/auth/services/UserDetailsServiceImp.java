package io.netiot.auth.services;

import io.netiot.auth.exceptions.AuthInternErrorException;
import io.netiot.auth.exceptions.UnactivatedUserException;
import io.netiot.auth.feign.UserEndpoints;
import io.netiot.auth.models.JwtUserModel;
import io.netiot.auth.models.SpringUserDetails;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service(value="userDetailsService")
public class UserDetailsServiceImp implements UserDetailsService {

    private final UserEndpoints userEndpoints;

    public UserDetailsServiceImp(final UserEndpoints userEndpoints) {
        this.userEndpoints = Objects.requireNonNull(userEndpoints,"userEndpoints must not be null.");
    }

    @Override
    public UserDetails loadUserByUsername(final String email) throws AuthenticationException {
        List<JwtUserModel> jwtUserModelList = userEndpoints.getJwtUser(email);
        if(jwtUserModelList == null){
            throw new AuthInternErrorException("Server error, please try again later.");
        }
        if(jwtUserModelList.isEmpty()){
            throw new UsernameNotFoundException("User with email " + email + " was not found in the database");
        }

        JwtUserModel jwtUserModel = jwtUserModelList.get(0);

        if(!jwtUserModel.getEnabled()){
            throw new UnactivatedUserException("User account is not activated.");
        }

        return SpringUserDetails.builder()
                .id(jwtUserModel.getId())
                .firstName(jwtUserModel.getFirstName())
                .lastName(jwtUserModel.getLastName())
                .email(jwtUserModel.getEmail())
                .password(jwtUserModel.getPassword())
                .active(jwtUserModel.getActive())
                .enabled(jwtUserModel.getEnabled())
                .role(jwtUserModel.getRole())
                .grantedAuthorityList(getGrantedAuthorityList(jwtUserModel.getRole()))
                .organizationId(jwtUserModel.getOrganizationId())
                .organizationName(jwtUserModel.getOrganizationName())
                .build();
    }

    private List<GrantedAuthority> getGrantedAuthorityList(final String role) {
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role);
        List<GrantedAuthority> authorityList = new ArrayList<>();
        authorityList.add(grantedAuthority);
        return authorityList;
    }

}
