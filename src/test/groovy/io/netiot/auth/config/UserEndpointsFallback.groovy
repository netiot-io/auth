package io.netiot.auth.config

import io.netiot.auth.feign.UserEndpoints
import io.netiot.auth.generators.JwtUserModelGenerator
import io.netiot.auth.models.JwtUserModel

class UserEndpointsFallback implements UserEndpoints {

    @Override
    List<JwtUserModel> getJwtUser(final String email) {
        return Arrays.asList(JwtUserModelGenerator.aJwtUserModel())
    }

}
